# README #

CodeCanon Hackathon

written by Tome Software under NDA with Canon USA.

### Overview of this codebase ###

* hackathon-demo - (client/server) node.js app showing basic functionality of Canon MM100-WS
* howielib - Node PTP-IP API wrapper for Canon MM100-WS  **required for hackathon-demo**
